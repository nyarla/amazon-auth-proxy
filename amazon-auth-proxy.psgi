#!/usr/bin/perl

use strict;
use warnings;

package  AmazonAuthProxy;

our $VERSION = '0.02';

use URI::Amazon::APA;
use Plack::Request;

sub env_value {
    my ( $key ) = @_;

    my $prefix  = uc __PACKAGE__;
       $key     = uc $key;
    my $env     = "${prefix}_${key}";

    if ( exists $ENV{$env} ) {
        return $ENV{$env};
    }

    return;
}

my $locale  = env_value('locale') or die "Amazon API locale is not set.";
my $apikey  = env_value('apikey') or die "Amazon API key is not set.";
my $secret  = env_value('secret') or die "Amazon API secret key is not set.";

my %apipoint = ();
for my $locale (qw( ca de fr jp uk us )) {
    $apipoint{$locale} = {
        rest => "http://ecs.amazonaws.${locale}/onca/xml",
        xslt => "http://xml-${locale}.amznxslt.com/onca/xml",
    };
}

die "Unknown locale: ${locale}" if ( ! exists $apipoint{$locale} );
my ( $rest, $xslt ) = @{ $apipoint{$locale} }{qw( rest xslt )};

return sub {
    my $req     = Plack::Request->new($_[0]);
    my %params  = %{ $req->query_parameters };

    if ( ! exists $params{'Timestamp'} ) {
        my ( $sec, $min, $hr, $da, $mo, $yr ) = gmtime();
        $yr += 1900;
        $mo++;
        $params{'Timestamp'} = sprintf('%04d-%02d-%02dT%02d:%02d:%02dZ', $yr, $mo, $da, $hr, $min, $sec);
    }

    $params{'AWSAccessKey'} = $apikey;

    my $uri = URI::Amazon::APA->new( ( exists $params{'Style'} ) ? $xslt : $rest  );
       $uri->query_form( %params );
       $uri->sign( key => $apikey, secret => $secret );

    return [
        303,
        [
            'Location' => "${uri}",
        ],
        []
    ];
};

__END__

=head1 NAME

Amazon Auth proxy

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>thotep@nyarla.netE<gt>

=head1 LICENSE

public domain

=cut

