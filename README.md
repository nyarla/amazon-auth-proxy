## Read Me

## NAME

amazon-auth-proxy.psgi - Amazon Product Advertising API Authentication Proxy

## Required modules

* Plack (plackup required.)
* Plack::Request
* URI::Amazon::APA

## How to use

### 1. git clone

    $ git clone git://github.com/nyarla/amazon-auth-proxy.git

### 2. set env

    $ export AMAZONAUTHPROXY_LOCALE='us';
    $ export AMAZONAUTHPROXY_APIKEY='your APAAPI key here';
    $ export AMAZONAUTHPROXY_SECRET='your APAAPI secret key here';

### 3. run psgi script

    $ plackup --app amazon-auth-proxy.psgi

